# README #

**smdemo** - test Facebook app. Temporary  url: https://smdemo.umerov.info/

Facebook App is not published, available only for admins and testers (need permission 'user_posts')

### Installation  ###

* composer install
* copy .env.sample -> .env and setup credentials

### Run tests  ###

* ./vendor/bin/phpunit tests/

All of scripts produce HTML output, which you can send ot email or publish in internal support website.


---

```

PHP Assignment

Please do not use any existing framework.

1. Create an app in Facebook or VK, obtain the necessary credentials
2. Write code to authorize a user and get their own posts for the last 6 months in Facebook this is /me/posts (VK likely has something similar)
3. Show stats on the following:

a. Total Posts over that period
b. Average word length / post 
c. Longest post
e. Average posts split by month
f. Median number of posts in each month

4. Design the above to be generic and extendable to similar Facebook / VK endpoints and for maintenance by other staff members
```