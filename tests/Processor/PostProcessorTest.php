<?php
/**
 * File contains Class PostProcessorTest
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Tests\Processor;

use Demo\Dto\PostDto;
use Demo\Processor\PostProcessor;
use Demo\Processor\StatFunction\TotalPostsFunction;

/**
 * Class PostProcessorTest
 *
 * @package Tests\Processor\StatFunction
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class PostProcessorTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var PostProcessor
     */
    protected $processor;

    public function testRegular()
    {
        $functionMock = $this->getMockBuilder(TotalPostsFunction::class)
                             ->setMethods(['handle', 'getResult'])
                             ->getMock();

        $functionMock->expects($this->once())
                     ->method('handle');

        $functionMock->expects($this->once())
                     ->method('getResult');

        $this->processor->addHandler($functionMock);
        $this->processor->handle(new PostDto());

        $this->processor->getResult();
    }

    protected function setUp()
    {
        parent::setUp();

        $this->processor = new PostProcessor();
    }

}
