<?php
/**
 * File contains Class AveragePostByMonthFunctionTest
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Tests\Processor\StatFunction;

use Demo\Dto\PostDto;
use Demo\Processor\StatFunction\AveragePostByMonthFunction;
use Demo\Processor\StatFunction\StatFunctionInterface;

/**
 * Class AveragePostByMonthFunctionTest
 *
 * @package Tests\Processor\StatFunction
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class AveragePostByMonthFunctionTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var StatFunctionInterface
     */
    protected $function;

    public function testEmptyCollection()
    {
        $data = $this->function->getResult();
        $this->assertSame([AveragePostByMonthFunction::KEY => []], $data);
    }

    public function testRegular()
    {
        // 11, 9, 3, 5, 5
        $this->function->handle(
            (new PostDto())->setMessage('01234567891')->setCreated(\DateTime::createFromFormat('Y-m-d', '2018-01-01'))
        );
        $this->function->handle(
            (new PostDto())->setMessage('123456789')->setCreated(\DateTime::createFromFormat('Y-m-d', '2018-01-01'))
        );
        $this->function->handle(
            (new PostDto())->setMessage('123')->setCreated(\DateTime::createFromFormat('Y-m-d', '2018-01-01'))
        );
        $this->function->handle(
            (new PostDto())->setMessage('12345')->setCreated(\DateTime::createFromFormat('Y-m-d', '2018-01-01'))
        );
        $this->function->handle(
            (new PostDto())->setMessage('12345')->setCreated(\DateTime::createFromFormat('Y-m-d', '2018-01-01'))
        );
        $data = $this->function->getResult();
        $this->assertSame(
            [
                AveragePostByMonthFunction::KEY => [
                    '2018-01' => 6.6,
                ],
            ],
            $data
        );
    }

    protected function setUp()
    {
        parent::setUp();

        $this->function = new AveragePostByMonthFunction();
    }

}
