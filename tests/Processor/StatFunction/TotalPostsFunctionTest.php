<?php
/**
 * File contains Class TotalPostsFunctionTest
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Tests\Processor\StatFunction;

use Demo\Dto\PostDto;
use Demo\Processor\StatFunction\StatFunctionInterface;
use Demo\Processor\StatFunction\TotalPostsFunction;

/**
 * Class TotalPostsFunctionTest
 *
 * @package Tests\Processor\StatFunction
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class TotalPostsFunctionTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var StatFunctionInterface
     */
    protected $function;

    public function testEmptyCollection()
    {
        $data = $this->function->getResult();
        $this->assertSame([TotalPostsFunction::KEY => 0], $data);
    }

    public function testRegular()
    {
        $this->function->handle(new PostDto());
        $this->function->handle(new PostDto());
        $data = $this->function->getResult();
        $this->assertSame(
            [
                TotalPostsFunction::KEY => 2,
            ],
            $data
        );
    }

    protected function setUp()
    {
        parent::setUp();

        $this->function = new TotalPostsFunction();
    }

}
