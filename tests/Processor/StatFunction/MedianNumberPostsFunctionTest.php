<?php
/**
 * File contains Class MedianNumberPostsFunctionTest
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Tests\Processor\StatFunction;

use Demo\Dto\PostDto;
use Demo\Processor\StatFunction\MedianNumberPostsFunction;
use Demo\Processor\StatFunction\StatFunctionInterface;

/**
 * Class MedianNumberPostsFunctionTest
 *
 * @package Tests\Processor\StatFunction
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class MedianNumberPostsFunctionTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var StatFunctionInterface
     */
    protected $function;

    public function testEmptyCollection()
    {
        $data = $this->function->getResult();
        $this->assertSame([MedianNumberPostsFunction::KEY => []], $data);
    }

    public function testRegular()
    {
        // 11, 9, 3, 5, 5
        $this->function->handle(
            (new PostDto())->setMessage('01234567891')->setCreated(\DateTime::createFromFormat('Y-m-d', '2018-01-01'))
        );
        $this->function->handle(
            (new PostDto())->setMessage('123456789')->setCreated(\DateTime::createFromFormat('Y-m-d', '2018-01-01'))
        );
        $this->function->handle(
            (new PostDto())->setMessage('123')->setCreated(\DateTime::createFromFormat('Y-m-d', '2018-01-01'))
        );
        $this->function->handle(
            (new PostDto())->setMessage('12345')->setCreated(\DateTime::createFromFormat('Y-m-d', '2018-01-01'))
        );
        $this->function->handle(
            (new PostDto())->setMessage('12345')->setCreated(\DateTime::createFromFormat('Y-m-d', '2018-01-01'))
        );
        $data = $this->function->getResult();
        $this->assertSame(
            [
                MedianNumberPostsFunction::KEY => [
                    '2018-01' => 5,
                ],
            ],
            $data
        );
    }

    protected function setUp()
    {
        parent::setUp();

        $this->function = new MedianNumberPostsFunction();
    }

}
