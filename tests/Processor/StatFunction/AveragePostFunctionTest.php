<?php
/**
 * File contains Class AveragePostFunctionTest
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Tests\Processor\StatFunction;

use Demo\Dto\PostDto;
use Demo\Processor\StatFunction\AveragePostFunction;
use Demo\Processor\StatFunction\StatFunctionInterface;

/**
 * Class AveragePostFunctionTest
 *
 * @package Tests\Processor\StatFunction
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class AveragePostFunctionTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var StatFunctionInterface
     */
    protected $function;

    public function testEmptyCollection()
    {
        $data = $this->function->getResult();
        $this->assertSame([AveragePostFunction::KEY => []], $data);
    }

    public function testRegular()
    {
        $this->function->handle((new PostDto())->setMessage('a aaaa')->setId(1));
        $this->function->handle((new PostDto())->setMessage('b bbbbbbbbb')->setId(2));
        $data = $this->function->getResult();
        $this->assertSame(
            [
                AveragePostFunction::KEY => [
                    1 => 2.5,
                    2 => 5,
                ],
            ],
            $data
        );
    }

    protected function setUp()
    {
        parent::setUp();

        $this->function = new AveragePostFunction();
    }

}
