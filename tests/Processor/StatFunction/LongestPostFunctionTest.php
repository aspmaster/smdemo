<?php
/**
 * File contains Class LongestPostFunctionTest
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Tests\Processor\StatFunction;

use Demo\Dto\PostDto;
use Demo\Processor\StatFunction\LongestPostFunction;
use Demo\Processor\StatFunction\StatFunctionInterface;

/**
 * Class LongestPostFunctionTest
 *
 * @package Tests\Processor\StatFunction
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class LongestPostFunctionTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var StatFunctionInterface
     */
    protected $function;

    public function testEmptyCollection()
    {
        $data = $this->function->getResult();
        $this->assertSame([], $data);
    }

    public function testRegular()
    {
        $this->function->handle((new PostDto())->setMessage('a')->setId(1));
        $this->function->handle((new PostDto())->setMessage('b bbbbbbbbb')->setId(2));
        $data = $this->function->getResult();
        $this->assertSame(
            [
                LongestPostFunction::KEY =>
                    [
                        'id'      => 2,
                        'message' => 'b bbbbbbbbb',
                        'created' => null,
                    ],
            ],
            $data
        );
    }

    protected function setUp()
    {
        parent::setUp();

        $this->function = new LongestPostFunction();
    }

}
