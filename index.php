<?php

require_once 'vendor/autoload.php';

$dotenv = new \Dotenv\Dotenv(__DIR__);
$dotenv->load();

$driver = new \Demo\Driver\FacebookDriver(new \GuzzleHttp\Client());

$processor = new \Demo\Processor\PostProcessor();
$processor->addHandler(new \Demo\Processor\StatFunction\AveragePostByMonthFunction());
$processor->addHandler(new \Demo\Processor\StatFunction\AveragePostFunction());
$processor->addHandler(new \Demo\Processor\StatFunction\LongestPostFunction());
$processor->addHandler(new \Demo\Processor\StatFunction\MedianNumberPostsFunction());
$processor->addHandler(new \Demo\Processor\StatFunction\TotalPostsFunction());

$isValid = false;

if (isset($_SESSION['access_token'])) {
    $driver->setAccessToken($_SESSION['access_token']);
    try {
        $driver->ping();
        $isValid = true;
    } catch (\Throwable $exception) {
        unset($_SESSION['access_token']);
    }
}

if (!$isValid && isset($_REQUEST['code'])) {
    $code = $_REQUEST['code'];
    try {
        $isValid = $driver->validateAccessToken($code);
    } catch (\Throwable $exception) {
        $isValid = false;
    }
}

if (!$isValid) {
    $redirectUri = $driver->getRedirectUri();
    header(sprintf('Location: %s', $redirectUri));
    die();
}

$result = $driver->getPosts(new \Demo\Dto\ParametersDto());
foreach ($result as $item) {
    $processor->handle($item);
}

echo '<pre>';
echo json_encode($processor->getResult(), JSON_PRETTY_PRINT);
echo '</pre>';



