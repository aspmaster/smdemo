<?php
/**
 * File contains Class PostConverter
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Converter;

use Demo\Dto\PostDto;

/**
 * Class PostConverter
 *
 * @author Albert Umerov <a.umerov@gmail.com>
 */
class PostConverter
{

    public static function fromFacebook(array $data)
    {
        $dto = new PostDto();

        $dto->setId($data['id']);
        $dto->setMessage($data['message'] ?? '');
        $dto->setCreated(\DateTime::createFromFormat(DATE_ATOM, $data['created_time']));

        return $dto;
    }

}
