<?php
/**
 * File contains Class PostProcessor
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Processor;

use Demo\Dto\PostDto;
use Demo\Processor\StatFunction\StatFunctionInterface;

/**
 * Class PostProcessor
 *
 * @package Processor
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class PostProcessor implements StatFunctionInterface
{
    /**
     * @var StatFunctionInterface[]
     */
    private $handlers = [];

    /**
     * @param StatFunctionInterface $handler
     */
    public function addHandler(StatFunctionInterface $handler)
    {
        $this->handlers[] = $handler;
    }

    /**
     * @param PostDto $post
     *
     * @return mixed|void
     */
    public function handle(PostDto $post)
    {
        foreach ($this->handlers as $handler) {
            $handler->handle($post);
        }
    }

    /**
     * @return array
     */
    public function getResult()
    {
        $result = [];

        foreach ($this->handlers as $handler) {
            $result = array_replace_recursive($result, $handler->getResult());
        }

        return $result;
    }

}
