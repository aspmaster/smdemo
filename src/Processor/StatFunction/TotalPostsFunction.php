<?php
/**
 * File contains Class TotalPostsFunction
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Processor\StatFunction;

use Demo\Dto\PostDto;

/**
 * Class TotalPostsFunction
 *
 * @package Demo\Processor\StatFunction
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class TotalPostsFunction implements StatFunctionInterface
{
    const KEY = 'totalPostsCount';

    /**
     * @var int
     */
    private $count = 0;

    /**
     * @param PostDto $post
     *
     * @return mixed|void
     */
    public function handle(PostDto $post)
    {
        $this->count++;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return [
            self::KEY => $this->count,
        ];
    }

}
