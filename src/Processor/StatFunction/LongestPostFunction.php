<?php
/**
 * File contains Class LongestPostFunction
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Processor\StatFunction;

use Demo\Dto\PostDto;

/**
 * Class LongestPostFunction
 *
 * @package Demo\Processor\StatFunction
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class LongestPostFunction implements StatFunctionInterface
{
    const KEY = 'longestPost';

    /**
     * @var int
     */
    private $maxLength = 0;

    /**
     * @var
     */
    private $longestPost;

    /**
     * @param PostDto $post
     *
     * @return mixed|void
     */
    public function handle(PostDto $post)
    {
        $currentLength = mb_strlen($post->getMessage());

        if ($currentLength > $this->maxLength) {
            $this->maxLength   = $currentLength;
            $this->longestPost = $post;
        }
    }

    /**
     * @return array
     */
    public function getResult()
    {
        if ($this->longestPost instanceof PostDto) {
            return [
                self::KEY => PostDto::toArray($this->longestPost),
            ];
        }
        return [];
    }

}
