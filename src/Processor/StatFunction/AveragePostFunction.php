<?php
/**
 * File contains Class AveragePostFunction
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Processor\StatFunction;

use Demo\Dto\PostDto;

/**
 * Class AveragePostFunction
 *
 * @package Demo\Processor\StatFunction
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class AveragePostFunction implements StatFunctionInterface
{
    const KEY = 'averagePost';

    const RESULT_AS_ASSOCIATIVE = 1;

    /**
     * @var array
     */
    private $result = [];

    /**
     * @param PostDto $post
     *
     * @return mixed|void
     */
    public function handle(PostDto $post)
    {
        //NOTE: May be another realization, depends on business requirements
        $words = str_word_count((string)$post->getMessage(), self::RESULT_AS_ASSOCIATIVE);

        $count  = count($words);
        $length = array_reduce(
            $words,
            function ($sum, $word) {
                return $sum + mb_strlen($word);
            },
            0
        );

        $value = 0;

        if ($count !== 0) {
            $value = $length / $count;
        }
        $this->result[$post->getId()] = $value;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return [
            self::KEY => $this->result,
        ];
    }

}
