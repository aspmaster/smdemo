<?php
/**
 * File contains Class AveragePostByMonthFunction
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Processor\StatFunction;

use Demo\Dto\PostDto;

/**
 * Class AveragePostByMonthFunction
 *
 * @package Demo\Processor\StatFunction
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class AveragePostByMonthFunction implements StatFunctionInterface
{
    const KEY = 'averagePostByMonth';

    /**
     * @var array
     */
    private $result = [];

    /**
     * @param PostDto $post
     *
     * @return mixed|void
     */
    public function handle(PostDto $post)
    {
        $month = $post->getCreated()->format('Y-m');

        if (isset($this->result[$month])) {
            $this->result[$month]['count']  += 1;
            $this->result[$month]['length'] += mb_strlen($post->getMessage());
        } else {
            $this->result[$month] = [
                'count'  => 1,
                'length' => mb_strlen($post->getMessage()),
            ];
        }
    }

    /**
     * @return array
     */
    public function getResult()
    {
        $result = [];
        foreach ($this->result as $key => $value) {
            $result[$key] = $value['length'] / $value['count'];
        }
        return [
            self::KEY => $result,
        ];
    }

}
