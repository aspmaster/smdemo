<?php
/**
 * File contains Class MedianNumberPostsFunction
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Processor\StatFunction;

use Demo\Dto\PostDto;
use MathPHP\Statistics\Average;

/**
 * Class MedianNumberPostsFunction
 *
 * @package Demo\Processor\StatFunction
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class MedianNumberPostsFunction implements StatFunctionInterface
{
    const KEY = 'medianNumberPostsFunction';

    /**
     * @var array
     */
    private $result = [];

    /**
     * @param PostDto $post
     *
     * @return mixed|void
     */
    public function handle(PostDto $post)
    {
        $month = $post->getCreated()->format('Y-m');

        $this->result[$month][] = mb_strlen($post->getMessage());
    }

    /**
     * @return array
     */
    public function getResult()
    {
        $result = [];
        foreach ($this->result as $key => $value) {
            $result[$key] = Average::median($value);
        }
        return [
            self::KEY => $result,
        ];
    }
}
