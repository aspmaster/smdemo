<?php
/**
 * File contains Class StatFunction
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Processor\StatFunction;

use Demo\Dto\PostDto;

/**
 * Class StatFunction
 *
 * @author Albert Umerov <a.umerov@gmail.com>
 */
interface StatFunctionInterface
{

    /**
     * Process next portion of data
     *
     * @param PostDto $post
     *
     * @return mixed
     */
    public function handle(PostDto $post);

    /**
     * Return statistics
     *
     * @return array
     */
    public function getResult();
}
