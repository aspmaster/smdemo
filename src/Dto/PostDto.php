<?php
/**
 * File contains Class PostDto
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Dto;

/**
 * Class PostDto
 *
 * @package Demo\Dto
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class PostDto
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     *
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public static function toArray(PostDto $postDto)
    {
        return [
            'id'      => $postDto->getId(),
            'message' => $postDto->getMessage(),
            'created' => $postDto->getCreated() instanceof \DateTime ? $postDto->getCreated()->format(DATE_ATOM) : null,
        ];
    }

}
