<?php
/**
 * File contains Class ParametersDto
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Dto;

/**
 * Class ParametersDto
 *
 * @package Demo\Dto
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class ParametersDto
{
    private $numMonthShow = 6;

    /**
     * @return int
     */
    public function getNumMonthShow()
    {
        return $this->numMonthShow;
    }

    /**
     * @param int $numMonthShow
     *
     * @return $this
     */
    public function setNumMonthShow($numMonthShow)
    {
        $this->numMonthShow = (int)$numMonthShow;
        return $this;
    }

}
