<?php
/**
 * File contains Class DriverInterface
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Driver;

use Demo\Dto\ParametersDto;

/**
 * Class DriverInterface
 *
 * @package Demo\Driver
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
interface DriverInterface
{
    public function getPosts(ParametersDto $parameters);
}
