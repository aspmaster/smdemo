<?php
/**
 * File contains Class FacebookDriver
 *
 * @since  14.07.2018
 * @author Albert Umerov <a.umerov@gmail.com>
 */

namespace Demo\Driver;

use Demo\Converter\PostConverter;
use Demo\Dto\ParametersDto;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Class FacebookDriver
 *
 * @package Demo\Driver
 * @author  Albert Umerov <a.umerov@gmail.com>
 */
class FacebookDriver implements DriverInterface
{

    const BASE_URI       = 'https://www.facebook.com/v3.0';
    const GRAPH_BASE_URI = 'https://graph.facebook.com/v3.0';

    const ENDPOINT_DIALOG = '/dialog/oauth';
    const ENDPOINT_OAUTH  = '/oauth/access_token';
    const ENDPOINT_POST   = '/me/posts';
    const ENDPOINT_PING   = '/me';

    const REDIRECT_URI = 'https://smdemo.umerov.info/';

    const LIMIT_PER_REQUEST = 100;

    /**
     * @var Client Client
     */
    private $client;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * FacebookDriver constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Prepare string for redirecting to login and authorize app
     *
     * @return string
     */
    public function getRedirectUri()
    {
        $params = [
            'client_id'    => getenv('APP_ID'),
            'scope'        => 'user_posts',
            'redirect_uri' => self::REDIRECT_URI,
        ];

        return sprintf('%s%s?%s', self::BASE_URI, self::ENDPOINT_DIALOG, http_build_query($params));
    }

    /**
     * Check received code and retrieve access_token
     *
     * @param $code
     *
     * @return string|null
     */
    public function validateAccessToken($code)
    {
        $params = [
            'client_id'     => getenv('APP_ID'),
            'client_secret' => getenv('APP_SECRET'),
            'redirect_uri'  => self::REDIRECT_URI,
            'code'          => $code,
        ];

        $endpoint = sprintf('%s%s', self::GRAPH_BASE_URI, self::ENDPOINT_OAUTH);

        $response = $this->client->get($endpoint, ['query' => $params]);

        $result = $this->parseResponse($response);

        if (isset($result['access_token'])) {
            $this->accessToken        = $result['access_token'];
            $_SESSION['access_token'] = $this->accessToken;
            return true;
        }

        return false;
    }

    /**
     * Iterate over all user posts
     *
     * @param ParametersDto $parameters
     *
     * @return \Generator
     */
    public function getPosts(ParametersDto $parameters)
    {
        if (empty($this->accessToken)) {
            throw new \RuntimeException('Not authorized');
        }

        $params = [
            'access_token' => $this->accessToken,
            'limit'        => self::LIMIT_PER_REQUEST,
            'since'        => (new \DateTime())->modify(sprintf('-%s months', $parameters->getNumMonthShow()))->format(
                DATE_ATOM
            ),
        ];

        $endpoint = sprintf('%s%s', self::GRAPH_BASE_URI, self::ENDPOINT_POST);

        $response = $this->client->get($endpoint, ['query' => $params]);

        $result = $this->parseResponse($response);

        while (isset($result['data']) && !empty($result['data'])) {
            foreach ($result['data'] as $post) {
                yield PostConverter::fromFacebook($post);
            }
            unset($result['data']);
            if (isset($result['paging']['next'])) {
                $result = $this->parseResponse($this->client->get($result['paging']['next']));
            }
        }
    }

    /**
     * Check validity of access_token
     *
     * @throws \RuntimeException
     */
    public function ping()
    {
        if (empty($this->accessToken)) {
            throw new \RuntimeException('Not authorized');
        }

        $params = [
            'access_token' => $this->accessToken,
        ];

        $endpoint = sprintf('%s%s', self::GRAPH_BASE_URI, self::ENDPOINT_PING);

        $response = $this->client->get($endpoint, ['query' => $params]);

        $result = $this->parseResponse($response);

        if (isset($result['id'], $result['name'])) {
            return;
        }

        throw new \RuntimeException('Not authorized');
    }

    /**
     * @param string $accessToken
     *
     * @return $this
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @param ResponseInterface $response
     *
     * @return mixed
     */
    private function parseResponse(ResponseInterface $response)
    {
        $body = (string)$response->getBody();
        return json_decode($body, true);
    }
}
